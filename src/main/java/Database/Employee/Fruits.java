package Database.recommendationsystem.employee;

import java.util.ArrayList;
import java.util.List;

public class Fruits {

    List<String> fruits = new ArrayList<>();

    public Fruits() {
        fruits();
    }

    public String getFruit(int i) {
        return fruits.get(i);
    }

    private void fruits() {
        fruits.add("Abacate");
        fruits.add("Abacaxi");
        fruits.add("Abiu");
        fruits.add("Araçá");
        fruits.add("Azeitona");
        fruits.add("Açaí");
        fruits.add("Acerola");
        fruits.add("Ameixa");
        fruits.add("Amora");
        fruits.add("Abricó");
        fruits.add("Anonácea");
        fruits.add("Ananás");
        fruits.add("Abóbora");
        fruits.add("Babaco");
        fruits.add("Bacaba");
        fruits.add("Bacuri");
        fruits.add("Banana");
        fruits.add("Baru");
        fruits.add("Buriti");
        fruits.add("Bilimbi");
        fruits.add("Biribá");
        fruits.add("Butiá");
        fruits.add("Cabeludinha");
        fruits.add("Cacau");
        fruits.add("Cagaita");
        fruits.add("Cajá-manga");
        fruits.add("Cereja");
        fruits.add("Caimito");
        fruits.add("Cajá");
        fruits.add("Conde");
        fruits.add("Cupuaçu");
        fruits.add("Caju");
        fruits.add("Calabaça");
        fruits.add("Carambola");
        fruits.add("Calabura");
        fruits.add("Coco");
        fruits.add("Cambucá");
        fruits.add("Cagaita");
        fruits.add("Cambuci");
        fruits.add("Caqui");
        fruits.add("Damasco");
        fruits.add("Dovyalis");
        fruits.add("Durião");
        fruits.add("Embaúba");
        fruits.add("Feijoa");
        fruits.add("Figo");
        fruits.add("Framboesa");
        fruits.add("Fruta-pão");
        fruits.add("Frutas do cerrado");
        fruits.add("Fruta-do-conde");
        fruits.add("Glicosmis");
        fruits.add("Goiaba");
        fruits.add("Granadilla");
        fruits.add("Graviola");
        fruits.add("Groselha");
        fruits.add("Grumixama");
        fruits.add("Guabiju");
        fruits.add("Guabiroba");
        fruits.add("Guaraná");
        fruits.add("Guariroba");
        fruits.add("Heisteria");
        fruits.add("Himbeere");
        fruits.add("Ilama");
        fruits.add("Ingá");
        fruits.add("Jabuticaba");
        fruits.add("Jaca");
        fruits.add("Jambo");
        fruits.add("Jambolão");
        fruits.add("Jaracatiá");
        fruits.add("Jamelão");
        fruits.add("Jatobá");
        fruits.add("Jenipapo");
        fruits.add("Jerivá");
        fruits.add("Jujuba");
        fruits.add("Kiwi");
        fruits.add("Langsat");
        fruits.add("Laranja");
        fruits.add("Lichia");
        fruits.add("Limão");
        fruits.add("Limas");
        fruits.add("Longan");
        fruits.add("Lucuma");
        fruits.add("Mabolo");
        fruits.add("Maçã");
        fruits.add("Macadâmia");
        fruits.add("Mamey");
        fruits.add("Mamoncillo");
        fruits.add("Maná");
        fruits.add("Manga");
        fruits.add("Mangaba");
        fruits.add("Mangostão");
        fruits.add("Maracujá");
        fruits.add("Marmeladinha");
        fruits.add("Mamão");
        fruits.add("Marmelo");
        fruits.add("Marolo");
        fruits.add("Marula");
        fruits.add("Massala");
        fruits.add("Melancia");
        fruits.add("Melão");
        fruits.add("Mirtilo");
        fruits.add("Morango");
        fruits.add("Murici");
        fruits.add("Naranjilla");
        fruits.add("Nectarina");
        fruits.add("Nêspera");
        fruits.add("Noni");
        fruits.add("Noz Pecã");
        fruits.add("Olho-De-Boi");
        fruits.add("Pequi");
        fruits.add("Pêra");
        fruits.add("Pêssego");
        fruits.add("Physalis");
        fruits.add("Pinha");
        fruits.add("Pinhão");
        fruits.add("Pistache");
        fruits.add("Pitanga");
        fruits.add("Pitangão");
        fruits.add("Pitaya");
        fruits.add("Pitomba");
        fruits.add("Pulasan");
        fruits.add("Pupunha");
        fruits.add("Quina");
        fruits.add("Rambutão");
        fruits.add("Romã");
        fruits.add("Salak");
        fruits.add("Santol");
        fruits.add("Sapoti");
        fruits.add("Sapucaia");
        fruits.add("Sapucaia");
        fruits.add("Seriguela");
        fruits.add("Taiúva");
        fruits.add("Tâmara");
        fruits.add("Tamarindo");
        fruits.add("Tangerina");
        fruits.add("Tarumã");
        fruits.add("Tomate");
        fruits.add("Toranja");
        fruits.add("Umbu");
        fruits.add("Umê");
        fruits.add("Uva");
        fruits.add("Uvaia");
        fruits.add("Veludo");
        fruits.add("Xixá");
        fruits.add("Yamamomo");
        fruits.add("Zimbro");
        fruits.add("Zitrone");

    }

}
