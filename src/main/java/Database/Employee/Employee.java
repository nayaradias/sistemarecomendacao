package Database.Employee;

import Database.ConnectionDatabase;
import Database.FileReaderEmployee;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Random;

public class Employee {

    private Database.recommendationsystem.employee.Fruits fruits = new Database.recommendationsystem.employee.Fruits();
    private Random rand = new Random();
    private List<String> employees;
    private Connection connection = new ConnectionDatabase().conecta();


    public Employee() {
        FileReaderEmployee fileReaderEmployee = new FileReaderEmployee();
        employees = fileReaderEmployee.getDataSet();
    }

    private void employee() throws SQLException {
        for (String employee : employees) {

            String[] person = employee.split(";");
            saveEmployee(person);
        }
    }

    private void saveEmployee(String[] employee) throws SQLException {

        try {
            Statement stmnt = null;
            stmnt = connection.createStatement();

            String SQL = "INSERT INTO employee(" +
                    "employeeID, " +
                    "name, " +
                    "gender, " +
                    "role," +
                    "grade, " +
                    "hireDate, " +
                    "homeOffice," +
                    "currentWorkingOffice," +
                    "staffingOffice," +
                    "totalYearsOfEXP," +
                    "yearsOfEXP)" +
                    "VALUES ('" +
                    employee[0] + "','" +
                    fruits.getFruit(getNumber()) + "','" +
                    employee[2] + "','" +
                    employee[3] + "','" +
                    employee[4] + "','" +
                    employee[5] + "','" +
                    employee[6] + "','" +
                    employee[7] + "','" +
                    employee[8] + "','" +
                    employee[9] + "','" +
                    employee[10] + "')";

            stmnt.executeUpdate(SQL);
        } catch (Exception e) {

        }
    }

    private int getNumber() {
        return rand.nextInt(146);
    }
}
