package Database.Employee;

import Database.ConnectionDatabase;
import Database.FileReaderSkills;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Skill {

    private List<String> skills;
    private Connection connection = new ConnectionDatabase().conecta();

    public Skill() throws SQLException {
        FileReaderSkills fileReaderSkills = new FileReaderSkills();
        skills = fileReaderSkills.getDataSet();
        constructSkills();
    }

    private void constructSkills() throws SQLException {
        String stringSkills;
        String[] capabilities = new String[0];
        String[] firstCapability;
        String level;
        String idBefore = "";

        for (String skill : skills) {

            String[] line = skill.split(";");


            if (line[0].equals("")) {

                line[0] = idBefore;
            }

            if (line.length > 1) {

                stringSkills = line[1];
                capabilities = stringSkills.split(",");

                firstCapability = capabilities[0].split(":");
                level = formatLevel(firstCapability[0]);
                capabilities[0] = firstCapability[1];

                setDatabase(line[0], capabilities, level);
            }

            idBefore = line[0];

        }
    }

    private void setDatabase(String id, String[] capabilities, String level) throws SQLException {
        for (String capability : capabilities) {
            saveSkills(id, capability, level);
        }
    }

    private String formatLevel(String level) {
        level = level.replace("\"", "");
        level = level.replace("[", "");
        return level.replace("]", "");
    }

    private void saveSkills(String employeeId, String skill, String level) throws SQLException {

        try {
            Statement stmnt = null;
            stmnt = connection.createStatement();

            String SQL = "INSERT INTO skills(" +
                    "employeeid, " +
                    "levelskill, " +
                    "nameskill)" +
                    "VALUES ('" +
                    employeeId + "','" +
                    level + "','" +
                    skill + "')";

            stmnt.executeUpdate(SQL);
        } catch (Exception e) {

        }
    }


}
