package Database.Employee;

import Database.ConnectionDatabase;
import Database.FileReaderEmployeeProjectExperience;

import java.sql.*;
import java.util.List;

public class Project {

    private List<String> projects;
    private List<String> peopleProjects;
    private Connection connection = new ConnectionDatabase().conecta();

    public Project() {
        FileReaderEmployeeProjectExperience fileReaderEmployeeProjectExperience =
                new FileReaderEmployeeProjectExperience();

        projects = fileReaderEmployeeProjectExperience.getDataSet();

        includeEmployeeIdInProjectLine();
    }

    private void saveProject(String[] projects, String employeeId) {
        String projectName;
        for (String project : projects
        ) {
            projectName = changeCharacter(project);
            savePeopleProject(projectName, employeeId);
        }
    }

    private void savePeopleProject(String project, String employeeId) {
        try {
            Statement stmnt = null;
            stmnt = connection.createStatement();

            String SQL = "INSERT INTO experienceproject(" +
                    "employeeid, " +
                    "nameproject)" +
                    "VALUES ('" +
                    employeeId + "','" +
                    project + "')";

            stmnt.executeUpdate(SQL);

        } catch (Exception e) {

        }
    }

    private void includeEmployeeIdInProjectLine() {

        try {
            ResultSet resultSet = getEmployeeId();

            int i = 0;

            while (resultSet.next()) {

                String project = projects.get(i);
                String[] personProject = project.split(";");
                String employeeId = resultSet.getString(1);
                saveProject(personProject, employeeId);

                i++;
            }

        } catch (Exception e) {

        }
    }

    private ResultSet getEmployeeId() throws SQLException {
        String SQL = "select employeeid from employee";

        PreparedStatement pst = connection.prepareStatement(SQL);
        return pst.executeQuery();
    }

    private String changeCharacter(String string) {

        string = string.replace('A', 'O');
        string = string.replace('B', 'E');
        string = string.replace('C', 'R');
        string = string.replace('D', 'F');
        string = string.replace('E', 'S');
        string = string.replace('F', 'h');
        string = string.replace('G', 'G');
        string = string.replace('H', 'O');
        string = string.replace('I', 'K');
        string = string.replace('J', 'V');
        string = string.replace('K', '8');
        string = string.replace('L', 'o');
        string = string.replace('Y', 'o');
        string = string.replace('M', '2');
        string = string.replace('N', '_');
        string = string.replace('O', 'k');
        string = string.replace('P', 'G');
        string = string.replace('Q', 'S');
        string = string.replace('R', 'C');
        string = string.replace('S', 'G');
        string = string.replace('T', '8');
        string = string.replace('U', '*');
        string = string.replace('V', 'p');
        string = string.replace('X', 'u');
        string = string.replace('Z', 'i');
        string = string.replace('a', '1');
        string = string.replace('b', '3');
        string = string.replace('c', '5');
        string = string.replace('d', '6');
        string = string.replace('e', 't');
        string = string.replace('f', 'h');
        string = string.replace('g', 's');
        string = string.replace('h', 'g');
        string = string.replace('i', 'f');
        string = string.replace('j', '1');
        string = string.replace('k', '9');
        string = string.replace('l', 'o');
        string = string.replace('m', '2');
        string = string.replace('n', '_');
        string = string.replace('o', 'k');
        string = string.replace('p', 's');
        string = string.replace('q', 'l');
        string = string.replace('r', 'b');
        string = string.replace('s', '0');
        string = string.replace('t', '8');
        string = string.replace('y', '8');
        string = string.replace('u', '*');
        string = string.replace('v', 'p');
        string = string.replace('x', 'u');
        string = string.replace('z', 'i');


        return string;
    }
}
