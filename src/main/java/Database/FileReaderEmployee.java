package Database;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileReaderEmployee {

    public List<String> dataset = new ArrayList<>();

    public FileReaderEmployee() {
        run();
    }

    private void run() {

        File arquivoCSV = new File("/Users/nayaradias/TCC/RecommendationSystem/people.csv");

        try {
            String line;

            Scanner leitor = new Scanner(arquivoCSV);
            while (leitor.hasNext()) {
                line = leitor.nextLine();
                dataset.add(line);
            }

        } catch (FileNotFoundException e) {

        }
    }

    public List<String> getDataSet() {
        return dataset;
    }
}

