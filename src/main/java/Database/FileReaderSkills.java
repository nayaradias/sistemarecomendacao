package Database;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileReaderSkills {

    public List<String> dataset = new ArrayList<>();

    public FileReaderSkills() {
        run();
    }

    private void run() {

        File arquivoCSV = new File("/Users/nayaradias/TCC/RecommendationSystem/skills.csv");

        try {
            String line;

            Scanner leitor = new Scanner(arquivoCSV);
            while (leitor.hasNext()) {
                line = leitor.nextLine();
                dataset.add(line);
            }

        } catch (FileNotFoundException e) {

        }
    }

    public List<String> getDataSet() {
        return dataset;
    }
}
