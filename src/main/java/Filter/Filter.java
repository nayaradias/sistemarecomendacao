package Filter;

import Database.ConnectionDatabase;
import Recommendation.Attributes;
import Recommendation.AttributesFilter;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Filter {

    private Connection connection = new ConnectionDatabase().conecta();
    private Attributes attributes;
    private String project;

    public Filter(Attributes attributes) {
        this.attributes = attributes;
        this.project = attributes.getProjectName();
    }

    public ResultSet getResultSetByDatabase() throws SQLException {

        String SQL = "select * from beach " +
                "where nameproject = '" + attributes.getProjectName() + "'" +
                "group by (employeeid, android, aws, azure, bootstrap, \"c++\"" +
                ", communication, csharp, css, english, facilitation, java, negotiation" +
                ", portuguese, spanish, strategy, teaching, nameproject" +
                ", grade, homeoffice, staffingoffice, role)\n" +
                "order by employeeid";

        PreparedStatement pst = connection.prepareStatement(SQL);

        return pst.executeQuery();
    }

    public List getResultSetByDatabaseProfessional() throws SQLException {

        String SQL = "select distinct employeeid from beach " +
                " where nameproject = '" + project +
                "' order by employeeid";

        PreparedStatement pst = connection.prepareStatement(SQL);
        ResultSet resultSet = pst.executeQuery();

        List professionalOnProject = new ArrayList();

        while (resultSet.next()) {
           professionalOnProject.add(resultSet.getString("employeeid"));
        }

        return professionalOnProject;
    }

    public ResultSet getResultSetByDatabaseOnBeach() throws SQLException {

        String SQL = "select * from beach where nameproject = ''" +
                " order by employeeid";

        PreparedStatement pst = connection.prepareStatement(SQL);

        return pst.executeQuery();
    }
}