package Recommendation;

public class AttributesFilter {

    private String role;
    private String grade;
    private String cityVacancy;
    private String personVacancy;

    public AttributesFilter(String role, String grade, String cityVacancy, String personVacancy) {

        this.role = role;
        this.grade = grade;
        this.cityVacancy = cityVacancy;
        this.personVacancy = personVacancy;
    }

    public String getRole() {
        return role;
    }

    public String getGrade() {
        return grade;
    }

    public String getCityVacancy() {
        return cityVacancy;
    }

    public String getPersonVacancy() {
        return personVacancy;
    }
}
