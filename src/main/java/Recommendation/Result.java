package Recommendation;

import javafx.scene.layout.BackgroundImage;

import java.math.BigDecimal;

public class Result {

    private String professional;
    private BigDecimal similarValueProbability;

    public Result(String professional, BigDecimal similarValueProbability) {

        this.professional = professional;
        this.similarValueProbability = similarValueProbability;
    }

    public String getProfessional() {
        return professional;
    }

    public BigDecimal getSimilarValueProbability() {
        return similarValueProbability;
    }
}
