package Recommendation;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdviseService {

    private Attributes attributes;

    public AdviseService(Attributes attributes, AttributesFilter attributesFilter) throws Exception {
        this.attributes = attributes;
    }

    public List<NaiveBayesResult> recommendedItems() throws Exception {
        List<NaiveBayesResult> results = new NaiveBayesWeka(attributes).applyBayes();


        long zeros = results.stream().filter(result -> result.getHigher().compareTo(BigDecimal.ZERO) == 0).count();

        long higherZero = results.stream()
                .filter(result -> (result
                        .getHigher()
                        .compareTo(BigDecimal.ZERO) == 1)
                        &&
                        result
                                .getHigher()
                                .compareTo(new BigDecimal(0.60)) == -1)
                .count();

        long sixty = results.stream()
                .filter(result -> (result
                        .getHigher()
                        .compareTo(new BigDecimal(0.60)) == 1)
                        &&
                        result
                                .getHigher()
                                .compareTo(new BigDecimal(0.80)) == -1)
                .count();

        long Eighty = results.stream()
                .filter(result -> (result
                        .getHigher()
                        .compareTo(new BigDecimal(0.80)) == 1)
                        &&
                        result
                                .getHigher()
                                .compareTo(new BigDecimal(0.90)) == -1)
                .count();

        long ninety = results.stream()
                .filter(result -> (result
                        .getHigher()
                        .compareTo(new BigDecimal(0.90)) == 1)
                        &&
                        result
                                .getHigher()
                                .compareTo(new BigDecimal(0.95)) == -1)
                .count();

        long ninetyFive = results.stream()
                .filter(result -> (result
                        .getHigher()
                        .compareTo(new BigDecimal(0.95)) == 1)
                        &&
                        result
                                .getHigher()
                                .compareTo(new BigDecimal(0.98)) == -1)
                .count();

        long ninetyEight = results.stream()
                .filter(result -> (result
                        .getHigher()
                        .compareTo(new BigDecimal(0.98)) == 1)
                        &&
                        result
                                .getHigher()
                                .compareTo(new BigDecimal(1.0)) == -1)
                .count();

        long hundred = results.stream()
                .filter(result -> (result
                        .getHigher()
                        .compareTo(new BigDecimal(1.0)) == 0))
                .count();

        return null;
    }
}

