package Recommendation;

import java.math.BigDecimal;
import java.util.List;

public class Employee {

    private String employeeId;
    private String grade;
    private String homeOffice;
    private String role;
    private String android;
    private String aws;
    private String azure;
    private String bootstrap;
    private String cplusplus;
    private String communication;
    private String csharp;
    private String css;
    private String english;
    private String facilitation;
    private String java;
    private String negotiation;
    private String portuguese;
    private String strategy;
    private String spanish;
    private String teaching;

    public Employee(String employeeId,
                    String android,
                    String aws,
                    String azure,
                    String bootstrap,
                    String cplusplus,
                    String communication,
                    String csharp,
                    String css,
                    String english,
                    String facilitation,
                    String java,
                    String negotiation,
                    String portuguese,
                    String strategy,
                    String spanish,
                    String teaching,
                    String grade,
                    String homeOffice,
                    String role) {


        this.employeeId = employeeId;
        this.grade = grade;
        this.homeOffice = homeOffice;
        this.role = role;
        this.android = android;
        this.aws = aws;
        this.azure = azure;
        this.bootstrap = bootstrap;
        this.cplusplus = cplusplus;
        this.communication = communication;
        this.csharp = csharp;
        this.css = css;
        this.english = english;
        this.facilitation = facilitation;
        this.java = java;
        this.negotiation = negotiation;
        this.portuguese = portuguese;
        this.strategy = strategy;
        this.spanish = spanish;
        this.teaching = teaching;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getGrade() {
        return grade;
    }

    public String getRole() {
        return role;
    }

    public String getHomeOffice() {
        return homeOffice;
    }

    public String getAndroid() {
        return android;
    }

    public String getAws() {
        return aws;
    }

    public String getAzure() {
        return azure;
    }

    public String getBootstrap() {
        return bootstrap;
    }

    public String getCplusplus() {
        return cplusplus;
    }

    public String getCommunication() {
        return communication;
    }

    public String getCsharp() {
        return csharp;
    }

    public String getCss() {
        return css;
    }

    public String getEnglish() {
        return english;
    }

    public String getFacilitation() {
        return facilitation;
    }

    public String getJava() {
        return java;
    }

    public String getNegotiation() {
        return negotiation;
    }

    public String getPortuguese() {
        return portuguese;
    }

    public String getStrategy() {
        return strategy;
    }

    public String getSpanish() {
        return spanish;
    }

    public String getTeaching() {
        return teaching;
    }
}