package Recommendation;

import Filter.Filter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class NaiveBayesResult {

    private double[] probabilities;
    private String employeeId;
    private List peopleInProject;
    private List<Result> similarity = new ArrayList<>();
    private BigDecimal higher = BigDecimal.ZERO;
    private BigDecimal lower = BigDecimal.ZERO;

    public NaiveBayesResult(Employee employee, double[] probabilities, List peopleInProject) {

        this.probabilities = probabilities;
        this.employeeId = employee.getEmployeeId();

        this.peopleInProject = peopleInProject;

        buildResults();
    }

    public void buildResults() {
        BigDecimal[] probabilities = new BigDecimal[this.probabilities.length];
        BigDecimal higherValue = new BigDecimal(0.0);
        BigDecimal lowerValue = new BigDecimal(0.0);

        for (int i = 0; i < this.probabilities.length -1; i++) {
            probabilities[i] = new BigDecimal(this.probabilities[i]);

            String professional = peopleInProject.get(i).toString();
            similarity.add(new Result(professional, probabilities[i].setScale(3, RoundingMode.UP)));

            if(higherValue.compareTo(probabilities[i]) == -1) {
                higherValue = probabilities[i];
            }

            if(lowerValue.compareTo(probabilities[i]) == 1) {
                lowerValue = probabilities[i];
            }
        }

        higher = higherValue;
        lower = lowerValue;

    }

    public double[] getProbabilities() {
        return probabilities;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public List getPeopleOnProject() {
        return peopleInProject;
    }

    public List<Result> getSimilarity() {
        Comparator<Result> resultComparator = (Comparator<Result>) (n1, n2) -> n1.getSimilarValueProbability()
                .compareTo(n2.getSimilarValueProbability());

        return similarity.stream().sorted(resultComparator.reversed()).collect(Collectors.toList());
    }

    public BigDecimal getHigher() {
        return higher;
    }

    public BigDecimal getLower() {
        return lower;
    }
}
