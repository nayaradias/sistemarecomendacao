package Recommendation;

public class Attributes {

    private final String projectName;
    private String[] technology;
    private String[] language;
    private String[] consulting;

    public Attributes(String projectName, String[] technology, String[] language, String... consulting) {
        this.projectName = projectName;

        this.technology = technology;
        this.language = language;
        this.consulting = consulting;
    }

    public String getProjectName() {
        return projectName;
    }

    public String[] getTechnology() {
        return technology;
    }

    public String[] getLanguage() {
        return language;
    }

    public String[] getConsulting() {
        return consulting;
    }
}
