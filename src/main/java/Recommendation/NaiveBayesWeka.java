package Recommendation;


import Filter.Filter;
import com.opencsv.CSVWriter;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instances;
import weka.core.SparseInstance;
import weka.core.converters.ConverterUtils;
import weka.filters.unsupervised.attribute.NumericToNominal;
import weka.filters.unsupervised.attribute.Remove;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NaiveBayesWeka {

    private NaiveBayes naiveBayes;
    private Instances instances;
    private Attributes attributes;

    public NaiveBayesWeka(Attributes attributes) throws Exception {
        this.attributes = attributes;
        instances = createInstance();
        naiveBayes = new NaiveBayes();
    }

    public List<NaiveBayesResult> applyBayes() throws Exception {

        runNaiveBayes();
        List resultSetByDatabaseProfessional = new Filter(attributes).getResultSetByDatabaseProfessional();
        List<NaiveBayesResult> employeesResult = new ArrayList<>();
        List<Employee> peopleWithoutProjectInDatabase = getPeopleWithoutProjectInDatabase();

        for (int j = 0; j < peopleWithoutProjectInDatabase.size(); j++) {
            SparseInstance sparseInstance = new SparseInstance(19);
            sparseInstance.setDataset(instances);

            sparseInstance.setValue(1, peopleWithoutProjectInDatabase.get(j).getAndroid());
            sparseInstance.setValue(2, peopleWithoutProjectInDatabase.get(j).getAws());
            sparseInstance.setValue(3, peopleWithoutProjectInDatabase.get(j).getAzure());
            sparseInstance.setValue(4, peopleWithoutProjectInDatabase.get(j).getBootstrap());
            sparseInstance.setValue(5, peopleWithoutProjectInDatabase.get(j).getCplusplus());
            sparseInstance.setValue(6, peopleWithoutProjectInDatabase.get(j).getCommunication());
            sparseInstance.setValue(7, peopleWithoutProjectInDatabase.get(j).getCsharp());
            sparseInstance.setValue(8, peopleWithoutProjectInDatabase.get(j).getCss());
            sparseInstance.setValue(9, peopleWithoutProjectInDatabase.get(j).getEnglish());
            sparseInstance.setValue(10, peopleWithoutProjectInDatabase.get(j).getFacilitation());
            sparseInstance.setValue(11, peopleWithoutProjectInDatabase.get(j).getJava());
            sparseInstance.setValue(12, peopleWithoutProjectInDatabase.get(j).getNegotiation());
            sparseInstance.setValue(13, peopleWithoutProjectInDatabase.get(j).getPortuguese());
            sparseInstance.setValue(14, peopleWithoutProjectInDatabase.get(j).getSpanish());
            sparseInstance.setValue(15, peopleWithoutProjectInDatabase.get(j).getStrategy());
            sparseInstance.setValue(16, peopleWithoutProjectInDatabase.get(j).getTeaching());

            double[] probabilities = naiveBayes.distributionForInstance(sparseInstance);

            employeesResult.add(new NaiveBayesResult(peopleWithoutProjectInDatabase.get(j), probabilities, resultSetByDatabaseProfessional));
        }

        return employeesResult;
    }

    public void runNaiveBayes() throws Exception {

        NumericToNominal filter = new NumericToNominal();

        filter.setInputFormat(instances);

        instances = weka.filters.Filter.useFilter(instances, filter);

        int[] indexes = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

        Remove removeFilter = new Remove();
        removeFilter.setAttributeIndicesArray(indexes);
        removeFilter.setInvertSelection(true);
        removeFilter.setInputFormat(instances);

        instances = weka.filters.Filter.useFilter(instances, removeFilter);

        instances.setClassIndex(0);
        naiveBayes.buildClassifier(instances);

        System.out.print(naiveBayes);
    }

    private Instances createInstance() throws Exception {
        createCsvFile();
        ConverterUtils.DataSource dataSource =
                new ConverterUtils.DataSource("src/main/java/filetobuildthearff.csv");

        return dataSource.getDataSet();
    }

    private void createCsvFile() throws IOException, SQLException {
        Writer writer = Files.newBufferedWriter(Paths.get("src/main/java/filetobuildthearff.csv"));
        CSVWriter csvWriter = new CSVWriter(writer);

        ResultSet resultSetByDatabase = new Filter(attributes).getResultSetByDatabase();

        String[] wantToLearn = {"invalid", "Want to learn", "Want to learn", "Want to learn", "Want to learn",
                "Want to learn", "Want to learn", "Want to learn", "Want to learn",
                "Want to learn", "Want to learn", "Want to learn", "Want to learn", "Want to learn",
                "Want to learn","Want to learn", "Want to learn", "","", "", "", ""};

        String[] expert = {"invalid", "Expert", "Expert", "Expert", "Expert",
                "Expert", "Expert", "Expert", "Expert",
                "Expert", "Expert", "Expert", "Expert",
                "Expert", "Expert","Expert", "Expert", "","", "", "",""};

        String[] moderate = {"invalid", "Moderate", "Moderate", "Moderate", "Moderate",
                "Moderate", "Moderate", "Moderate", "Moderate",
                "Moderate", "Moderate", "Moderate", "Moderate",
                "Moderate", "Moderate","Moderate", "Moderate", "","", "", "", ""};

        String[] experienced = {"invalid", "Experienced", "Experienced", "Experienced", "Experienced",
                "Experienced", "Experienced", "Experienced", "Experienced",
                "Experienced", "Experienced", "Experienced", "Experienced",
                "Experienced", "Experienced","Experienced", "Experienced", "","", "", "", ""};

        String[] beginner = {"invalid", "Beginner", "Beginner", "Beginner", "Beginner",
                "Beginner", "Beginner", "Beginner", "Beginner",
                "Beginner", "Beginner", "Beginner", "Beginner", "Beginner", "Beginner","Beginner",
                "Beginner", "","", "", "", ""};

        csvWriter.writeAll(resultSetByDatabase, true);
        csvWriter.writeNext(wantToLearn);
        csvWriter.writeNext(expert);
        csvWriter.writeNext(experienced);
        csvWriter.writeNext(moderate);
        csvWriter.writeNext(beginner);

        csvWriter.flush();
        writer.close();
    }

    public List getPeopleWithoutProjectInDatabase() throws SQLException {
        ResultSet resultSetByDatabase = new Filter(attributes).getResultSetByDatabaseOnBeach();

        return buildEmployees(resultSetByDatabase);
    }

    private List buildEmployees(ResultSet resultSetByDatabase) throws SQLException {
        List<Employee> employees = new ArrayList<>();
        while (resultSetByDatabase.next()) {

            employees.add(new Employee(resultSetByDatabase.getArray("employeeid").toString(),
                    (Objects.nonNull(resultSetByDatabase.getArray("android"))
                            ? resultSetByDatabase.getArray("android").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("aws"))
                            ? resultSetByDatabase.getArray("aws").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("azure"))
                            ? resultSetByDatabase.getArray("azure").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("bootstrap"))
                            ? resultSetByDatabase.getArray("bootstrap").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("c++"))
                            ? resultSetByDatabase.getArray("c++").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("communication"))
                            ? resultSetByDatabase.getArray("communication").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("csharp"))
                            ? resultSetByDatabase.getArray("csharp").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("css"))
                            ? resultSetByDatabase.getArray("css").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("english"))
                            ? resultSetByDatabase.getArray("english").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("facilitation"))
                            ? resultSetByDatabase.getArray("facilitation").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("java"))
                            ? resultSetByDatabase.getArray("java").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("negotiation"))
                            ? resultSetByDatabase.getArray("negotiation").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("portuguese"))
                            ? resultSetByDatabase.getArray("portuguese").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("strategy"))
                            ? resultSetByDatabase.getArray("strategy").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("spanish"))
                            ? resultSetByDatabase.getArray("spanish").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("teaching"))
                            ? resultSetByDatabase.getArray("teaching").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("homeoffice"))
                            ? resultSetByDatabase.getArray("homeoffice").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("grade"))
                            ? resultSetByDatabase.getArray("grade").toString() : ""),
                    (Objects.nonNull(resultSetByDatabase.getArray("role"))
                            ? resultSetByDatabase.getArray("role").toString() : "")));
        }

        return employees;
    }
}
