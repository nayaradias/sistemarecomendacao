package webRecommender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;


@SpringBootApplication
@ComponentScan("webRecommender")
public class BootGradleApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(BootGradleApplication.class, args);
    }
}