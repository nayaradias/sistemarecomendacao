package webRecommender;

import Recommendation.Attributes;
import Recommendation.AttributesFilter;
import Recommendation.AdviseService;
import Recommendation.Employee;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.util.List;

@Controller
@RequestMapping("/index")
public class ApplicationController {

    @RequestMapping(method = RequestMethod.GET)
    public String firstPage(Model model) throws SQLException {
        return "index";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String getEmployees(Model model,
                               @RequestParam(required = false, name = "role") String role,
                               @RequestParam(required = false, name = "experienceFilter") String grade,
                               @RequestParam(required = false, name = "projectName") String projectName,
                               @RequestParam(required = false, name = "cityVacancy") String cityVacancy,
                               @RequestParam(required = false, name = "personVacancy") String personVacancy,
                               @RequestParam(required = false, name = "approach") String approach,
                               @RequestParam(required = false, name = "technology") String[] technology,
                               @RequestParam(required = false, name = "language") String[] language,
                               @RequestParam(required = false, name = "consulting") String[] consulting) throws Exception {

        Attributes attributes = new Attributes(projectName, technology, language, consulting);
        AttributesFilter filter = new AttributesFilter(role, grade, cityVacancy, personVacancy);

        AdviseService recommederService = new AdviseService(attributes, filter);
        recommederService.recommendedItems();

        //model.addAttribute("results", results);
        model.addAttribute("roleFilter", role);
        model.addAttribute("experienceFilter", grade);
        model.addAttribute("projectNameFilter", projectName);
        model.addAttribute("cityVacancyFilter", cityVacancy);
        model.addAttribute("personVacancyFilter", personVacancy);
        model.addAttribute("approach", approach);

        return "index";
    }
}