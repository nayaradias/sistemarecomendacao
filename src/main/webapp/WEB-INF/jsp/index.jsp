<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html><html><head>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
      integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="https://raw.githubusercontent.com/savanihd/multi-select-autocomplete/master/style.css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="https://raw.githubusercontent.com/savanihd/multi-select-autocomplete/master/autocomplete.multiselect.js"></script>
<!------ Include the above in your HEAD tag ----------></head><body><nav class="navbar navbar-dark bg-dark"> <a class="navbar-brand" href="#">
    Recommendation System </a></nav><br><div class="col-md-12">
<div class="card border-left-info shadow h-100 py-2">
    <div class="card-body"><form:form method="POST" action="/index">
        <div class="row">

            <div class="col-md-2">
                <label>Role</label>
                <select class="form-control" id="role" name="role">
                    <option>${roleFilter}</option>
                    <option>Business Analist</option>
                    <option>Developer</option>
                    <option>Project Manager</option>
                    <option>Quality Analist</option>
                </select>
            </div>

            <div class="col-md-2">
                <label>ProjectName</label>
                <input type="text" class="form-control" id="projectName" name="projectName"
                       value="${projectNameFilter}" required>
            </div>

            <div class="col-md-2"><label>Grade</label>
                <select class="form-control" id="experienceFilter" name="experienceFilter">
                    <option>${experienceFilter}</option>
                    <option value="">All</option>
                    <option value="Consultant - Graduate">Consultant - Graduate</option>
                    <option value="Consultant">Consultant</option>
                    <option value="Senior Consultant">Senior Consultant</option>
                    <option value="Lead Consultant">Lead Consultant</option>
                    <option value="Principal Consultant">Principal Consultant</option>
                </select>
            </div>

            <div class="col-md-2">
                <label>Person city</label>
                <select class="form-control" id="cityVacancy" name="cityVacancy">
                    <option>${cityVacancyFilter}</option>
                    <option value="Belo Horizonte">Belo Horizonte</option>
                    <option value="Porto Alegre">Porto Alegre</option>
                    <option value="Recife">Recife</option>
                    <option value="São Paulo">Sao Paulo</option>
                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <label>Requirements</label>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label>Language</label>
                <select class="custom-select" name="language" multiple required>
                    <option value="English">English</option>
                    <option value="Spanish">Spanish</option>
                    <option value="Portuguese">Portuguese</option>
                </select>
            </div>
            <div class="col-md-4">
                <label>Consulting</label>
                <select class="custom-select" name="consulting" multiple>
                    <option value="Communication">Communication</option>
                    <option value="Facilitation">Facilitation</option>
                    <option value="Negotiation">Negotiation</option>
                    <option value="Teaching">Teaching</option>
                    <option value="Strategy">Strategy</option>
                </select>
            </div>
            <div class="col-md-4">
                <label>Technology</label>
                <select class="custom-select" name="technology" multiple required>
                    <option value="Android"> Android
                    <option value="AngularJS">AngularJS</option>
                    <option value="AWS"> AWS</option>
                    <option value="Azure"> Azure</option>
                    <option value="Bootstrap"> Bootstrap</option>
                    <option value="csharp"> C#</option>
                    <option value="c++"> C++</option>
                    <option value="CSS"> CSS</option>
                    <%--<option value="Google Chart"> Google Chart</option>--%>
                    <%--<option value="Haskell"> Haskell</option>--%>
                    <%--<option value="HTML"> HTML</option>--%>
                    <%--<option value="iOS"> iOS</option>--%>
                    <%--<option value="Java"> Java</option>--%>
                    <%--<option value="Javascript"> Javascript</option>--%>
                    <%--<option value="JQuery"> JQuery</option>--%>
                    <%--<option value="Kotlin"> Kotlin</option>n>--%>
                    <%--<option value="Microsoft SQL"> Microsoft SQL</option>--%>
                    <%--<option value="Mongo"> Mongo</option>--%>
                    <%--<option value="MySQL"> MySQL</option>--%>
                    <%--<option value="Node JS"> Node JS</option>--%>
                    <%--<option value="Oracle"> Oracle</option>--%>
                    <%--<option value="PHP"> PHP</option>--%>
                    <%--<option value="Postgres"> Postgres</option>--%>
                    <%--<option value="Python"> Python</option>--%>
                    <%--<option value="R"> R</option>--%>
                    <%--<option value="React"> React</option>--%>
                    <%--<option value="SQL"> SQL</option>--%>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1"><br> <input class="btn btn-dark" name="Recomendar" type="submit"></div>
        </div>
    </form:form></div>
</div>
</div><br>
<div class="col-md-12">

    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">value</th>
            <th scope="col">employee id</th>
            <th scope="col">grade</th>
            <th scope="col">role</th>
            <th scope="col">home office</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${results}" var="item">
            <tr>
                <th scope="row">${item.max}</th>
                <th scope="row">${item.employeeId}</th>
                <th scope="row">${item.grade}</th>
                <th scope="row">${item.role}</th>
                <th scope="row">${item.personVacancy}</th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<br>
</body>
</html>